﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TRG.Insight.ReportLibrary;
using log4net;      
using System.Data;
using System.Security.Permissions;
using System.Configuration;
using System.Runtime.InteropServices;
using System.Security.Principal;
using System.Net.Mail;
using System.IO;

namespace TRG.Insight.ReportSkeleton
{

    public class Skeleton : IReportLibrary
    {
        #region IReportLibrary Members

        private Logging obj;
        private ILog fileLog;
        //private ILog EmailLog;
        DatabaseHelper dbhelper;
        ExcelHelper excel;
        private string reportStatus = string.Empty;
        private string reportName = "Complete KPI Report";
      //  private string[] Recipient_list = new string[2] { "zanish.aslam@ibexglobal.com", "UmairTariq@ibexglobal.com" };
       // private string[] Recipient_list = new string[1] { "umair.tariq@ibexglobal.com"};


      //  private int prodsession;
        private string outputFilename;
        private DataTable dt;
        private DateTime reportDate;
        #endregion

        #region IReportLibrary Methods

        public Skeleton getObject()
        {
            return this;

        }
        public void Activate(string SessionId)
        {
            //STEP 1: Initialize configurations
            obj = new Logging();
            fileLog = obj.FileLogger(SessionId);
            this.reportName = Session.GetReportNameBySessionId(SessionId);
            this.reportName = "Complete KPI Report";
            //reportDate = DateTime.Now.AddDays(0);
            //if (reportDate.DayOfWeek == DayOfWeek.Sunday)
            //{
            //    return;
            //}
            //else if (reportDate.DayOfWeek == DayOfWeek.Monday)
            //{
            //    reportDate = DateTime.Now.AddDays(-2); // show saturday data
            //}
            //else
            //{
            //    reportDate = DateTime.Now.AddDays(-1);
            //}
            reportDate = DateTime.Now.AddDays(-1);
        }

        public void PerformDatabaseOperation(string SessionId)
        {
            try
            {
                fileLog.Debug(reportName + " starts");
                //STEP 2: Initialize database connection through configuration file, Config.xml
                fileLog.Debug("Going to get Database Helper");
                dbhelper = new DatabaseHelper(SessionId);
                fileLog.Debug("Database Helper recieved");

                ///////////////////////////////////DBHelper/////////////////////////////// 

                String query = dbhelper.ReadQueryfromFile("ReadSQL.txt");
                query = query.Replace("INPUTDATE", reportDate.ToShortDateString());
                this.dt = (System.Data.DataTable)dbhelper.ExecuteCommand(query, DBCommandType.DataTable, DBQueryType.SQLString, 0);

               // this.reportData = (System.Data.DataTable)dbhelper.ExecuteCommand(query, DBCommandType.DataTable, DBQueryType.SQLString, 0);
                dbhelper.CloseDatabase();
                fileLog.Debug("Database closed");
            }
            catch (Exception ex)
            {
                fileLog.Error("Exception occured: " + ex);
                //TRG.Insight.ReportSkeleton.Emailer.SendEMail(Recipient_list, "Error - NBTX_ISM_Transfer Outlier Report", ex.ToString(), reportDate.ToShortDateString());
            }

            
        }

        public void PerformExcelOperation(string SessionId)
        {
            ///////////////////////////////////Excel Helper//////////////////////////
           
            try
            {
            excel = new ExcelHelper(SessionId);
            //int processid = excel.InitializeExcel();
            fileLog.Debug("Excel object created");


               // TemplateFolderPath + "\\ExcelTemplate.xls"

           
                try
                {
                    var dirInfo = new DirectoryInfo(@"D:\TiVo\Report Process\Complete KPI Report\AgentYieldReportUVerse\bin\Debug\");
                    var filename = (from f in dirInfo.GetFiles("Complete KPI Report*.xlsx") orderby f.LastWriteTime descending select f).First();
                    excel.ExcelLoadTemplate(filename.Name);
                }
                catch (Exception ex)
                {
                    excel.ExcelLoadTemplate(@"\Template\ExcelTemplate.xlsx");
                }
            
                

                ////LoadTemplate
                // excel.ExcelLoadTemplate(@"\Template\ExcelTemplate.xlsx");
                //   System.Threading.Thread.Sleep((int)System.TimeSpan.FromSeconds(3).TotalMilliseconds);
                fileLog.Debug("Working on Sheet number 2");

            //excel.ExcelActivateSheet(4);

            excel.CreateExcelReport(this.dt.DataSet);
           // excel.ExcelFillCell(5, 2, dt.Tables[0]);

          //  excel.ExcelActivateSheet(1);
         //   excel.ExcelFillCell(1, 1, reportDate.ToShortDateString());

            this.outputFilename = reportName + ".xlsx";
            excel.Save(ref outputFilename, reportDate);
            fileLog.Debug("Saved");

            excel.EndReport();
            excel.ExcelConnectionClose();
            fileLog.Debug("ExcelConnectionClose");
     //    sendNotification(true, "", outputFilename);
            
            }
            catch (Exception ex)
            {
                fileLog.Error("Exception occured: " + ex);
                //TRG.Insight.ReportSkeleton.Emailer.SendEMail(Recipient_list, "Error - NBTX_ISM_Transfer Outlier Report", "EXCEL FUNCTION -- " + ex.ToString(), reportDate.ToShortDateString());
            }
        }

        public void FinalizeReport(string SessionId)
        {
            try
            {

                fileLog.Debug("ReportWork complete");
            }
            catch (Exception ex)
            {
                reportStatus = "F:" + ex;
                fileLog.Error("Exception occured: " + ex);
              //  EmailLog.Error("Error occured: " + ex);
            }
        }

        public void sendNotification(bool success, string message, string reportpath)
        {
            DateTime time = DateTime.Now.AddDays(-1);
            string strDate = time.ToString("u");
            strDate = strDate.Substring(0, 10);

            List<string> arrTo = new List<string>();


            //arrTo.Add("anam.naseem@ibexglobal.com");
            System.Net.Mail.MailMessage mail = new System.Net.Mail.MailMessage();
            mail.IsBodyHtml = true;

            mail.Subject = "Complete KPI Report";
            mail.From = new System.Net.Mail.MailAddress("noreply@ibexglobal.com");
         //   mail.To.Add("fatima.mehmood2@ibexglobal.com");
          //  mail.To.Add("kd2306@att.com");
            mail.To.Add("Ibextech.tivo@ibexglobal.com");
            //mail.To.Add("Ellisa.Woods@ftr.com");
            //mail.To.Add("allison.m.wirth@ftr.com");
            ////mail.To.Add("Kelly.Morgan@FTR.com");
            //mail.To.Add("Cheryl.Rowan@FTR.com");
            //mail.To.Add("jeffrey.d.mcnew@ftr.com");
            //mail.To.Add("Eric.Guarro@FTR.com");
            //mail.To.Add("Michael.Kavanagh@ibexglobal.com");
            //mail.To.Add("Alfy.Lee@ibexglobal.com");
            //mail.To.Add("Jason.Ritchey@ibexglobal.com");
            //mail.To.Add("frontierworkforce@ibexglobal.com");
            //mail.To.Add("IBEXtech.Frontier@ibexglobal.com");
            //mail.To.Add("paula.wicker@ftr.com");
            //mail.To.Add("richard.n.ornelas@ftr.com");
            //mail.To.Add("antonio.carrero@ftr.com");
            //mail.To.Add("rhonda.r.smith@ftr.com");

            message = "Please find Attached Complete KPI Report";

            mail.Body = message;
            if (reportpath != null)
            {
                mail.Attachments.Add(new Attachment(reportpath));
            }

            mail.Priority = System.Net.Mail.MailPriority.High;


            System.Net.Mail.SmtpClient smtpClient = new System.Net.Mail.SmtpClient("IBEXDC1-SMTP01.ibexglobal.com");
            //smtpClient.Timeout = 1000000;
            smtpClient.Port = 25;
            smtpClient.Credentials = new System.Net.NetworkCredential("Ser-smtpInsight", "SmTP-P@$sword4!z");
            smtpClient.Send(mail);


            //System.Net.Mail.SmtpClient smtpClient = new System.Net.Mail.SmtpClient("smtp-ext.trgworld.com");
            //smtpClient.Timeout = 1000000;
            //smtpClient.Send(mail);

        }
        #endregion
        
    }
}
