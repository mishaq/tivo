using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace TRG.Insight.ReportSkeleton
{
    public class Emailer
    {
        public  static void SendEMail_attachment(System.Net.Mail.Attachment[] atts,string [] Recipient_list, string theMsg, string msg,string ReportDate)
        {
           
            System.Net.Mail.SmtpClient obj = new System.Net.Mail.SmtpClient("trgmail01.trgworld.com", 25);

            System.Net.Mail.MailMessage mailmessage = new System.Net.Mail.MailMessage();
            mailmessage.From = new System.Net.Mail.MailAddress("Farmers Daily Performance <noreply@trgworld.com>");
            for (int i = 0; i < Recipient_list.Length; i++)
            {
                mailmessage.To.Add(Recipient_list[i]);
            }

            // mailmessage.To.Add("fatima.mehmood2@ibexglobal.com");
            //mailmessage.To.Add("Neil.Simmons@ibexglobal.com");
            //mailmessage.CC.Add("maaz.baig@ibexglobal.com");

       //     DateTime dateTimeNow = DateTime.Now.AddHours(-1);

        //    string dateTimeNowToString = dateTimeNow.ToString("yyyyMMdd_") + dateTimeNow.ToString("t").Replace(':', '_');
            mailmessage.Subject = "Farmers Daily Performance " + ReportDate;

            mailmessage.Body = msg + Environment.NewLine + Environment.NewLine  +theMsg;
            mailmessage.IsBodyHtml = true;

            //mailmessage.DeliveryNotificationOptions = System.Net.Mail.DeliveryNotificationOptions.OnSuccess;

            foreach (System.Net.Mail.Attachment var in atts)
            {
                mailmessage.Attachments.Add(var);
            }

            obj.Timeout = 200000000;

            obj.Send(mailmessage);
            

            mailmessage.Dispose();

        }
        public static void SendEMail(string[] Recipient_list, string Subject, string emailbody, string ReportDate)
        {

            System.Net.Mail.SmtpClient obj = new System.Net.Mail.SmtpClient("trgmail01.trgworld.com", 25);

            System.Net.Mail.MailMessage mailmessage = new System.Net.Mail.MailMessage();
            mailmessage.From = new System.Net.Mail.MailAddress("noreply@ibexglobal.com"); 
            for (int i = 0; i < Recipient_list.Length; i++)
            {
                mailmessage.To.Add(Recipient_list[i]);
            }
            mailmessage.Subject = Subject + "-" + ReportDate;
            mailmessage.Body = Environment.NewLine + Environment.NewLine + emailbody;
            mailmessage.IsBodyHtml = true;

            //mailmessage.DeliveryNotificationOptions = System.Net.Mail.DeliveryNotificationOptions.OnSuccess;
            obj.Timeout = 200000000;
            obj.Send(mailmessage);
            mailmessage.Dispose();

        }
    }
}
